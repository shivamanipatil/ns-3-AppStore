# Setting up Jenkins pipelines

This guide can be followed if you want to set up Jenkins pipelines used in the ns-3 appstore project.

## Pipeline names and their use

1. **module_with_bakefile** : Used to build modules with bakefile
2. **module_without_bakefile** : Used to build modules without bakefile
3. **fork_with_bakefile** : Used to build forks with bakefile
4. **dev_releases** : Used to build app releases using ns-3 dev

## Following things hold for all pipelines

1. Each of this job is a Jenkins pipeline job.
2. Each pipeline except **dev_releases** is parametrized.
3. Each pipeline is remotely built remotely on demand from appstore except **dev_releases** which is build periodically.
4. Pipelines call appstore REST API's so API endpoints used in Jenkinsfiles should point currently deployed appstore.
5. Also for these appstore REST API are auth protected so admin auth token should be generated from appstore and it should be stored as a **Jenkins creadential** named **appStoreSecret**.
6. Each pipeline requires two files :
   _ **Jenkinsfile** - This is the file actually executed by pipeline job.
   _ **Bash script** : This bash script is called inside Jenkinsfile and does the actual work of fetching things, building and testing.
   This files can be found in scripts/Jenkinsfiles and scripts/scripts_for_pipelines
7. **IMPORTANT** - After following steps mentioned below the local.env file used by appstore needs to be updated with Jenkins env variables. This enables appstore to communicate with Jenkins server.

## 1. module_with_bakefile

1. Create new pipeline from 'New Item' section from Jenkins and give it 'module_with_bakefile' name.
2. Select the 'This project is parameterized' option and add following parameters :
   - moduleName - (String)
   - moduleVersion - (String)
   - nsVersion - (String)
   - id - (String)
3. Select **Trigger builds remotely** option under **Build Triggers** section and add **Authentication Token**.
4. Copy **module_with_bakefile** Jenkinsfile from scripts/Jenkinsfile folder to pipeline section.

## 2. module_without_bakefile

1. Create new pipeline from 'New Item' section from Jenkins and give it 'module_without_bakefile' name.
2. Select the 'This project is parameterized' option and add following parameters :
   - url - (String)
   - nsVersion - (String)
   - tag - (String)
   - method - (String)
   - id - (String)
3. Select **Trigger builds remotely** option under **Build Triggers** section and add **Authentication Token**.
4. Copy **module_without_bakefile** Jenkinsfile from scripts/Jenkinsfile folder to pipeline section.

## 3. fork_with_bakefile

1. Create new pipeline from 'New Item' section from Jenkins and give it 'fork_with_bakefile' name.
2. Select the 'This project is parameterized' option and add following parameters :
   - forkName - (String)
   - forkVersion - (String)
   - nsVersion - (String)
   - id - (String)
3. Select **Trigger builds remotely** option under **Build Triggers** section and add **Authentication Token**.
4. Copy **fork_with_bakefile** Jenkinsfile from scripts/Jenkinsfile folder to pipeline section.

## 4. dev_releases

1. Create new pipeline from 'New Item' section from Jenkins and give it 'dev_releases' name.
2. Select **Build periodically** under **Build Triggers** and add the **Schedule** info e.g H 0 \* \* \* means every midnight build.
3. Copy **dev_release** Jenkinsfile from scripts/Jenkinsfile folder to pipeline section.

## Updating local.env file of appstore

After adding the pipelines to Jenkins. Appstore local.env file needs to updated with Jenkins creadentials and pipeline configurations. local.env file already contains all the required env variable required just their values need to added. Follow these steps :

1. **JENKINS_URL** : URL pointing to your Jenkins server. e.g localhost:port for local instance.
2. **JENKINS_USERNAME** : Jenkins account username.
3. **JENKINS_PASSWORD** : Jenkins account password or token. Token is preferred and can be generated through **Manage Jenkins** section.
4. **JENKINS_MODULE_BAKEFILE_PIPELINE** : Name given to module with bakefile pipeline. It is \*_module_with_bakefile_ if above conventions are followed.
5. **JENKINS_MODULE_NO_BAKEFILE_PIPELINE** : Name given to module without bakefile pipeline. It is \*_module_without_bakefile_ if above conventions are followed.
6. **JENKINS_FORK_BAKEFILE_PIPELINE** : Name given to fork with bakefile pipeline. It is \*_fork_with_bakefile_ if above conventions are followed.
7. **JENKINS_MODULE_BAKEFILE_TOKEN** : Pipeline remote trigger auth token for module with bakefile pipeline defined in per pipeline sections above.
8. **JENKINS_MODULE_NO_BAKEFILE_TOKEN** : Pipeline remote trigger auth token for module without bakefile pipeline defined in per pipeline sections above.
9. **JENKINS_FORK_BAKEFILE_TOKEN** : Pipeline remote trigger auth token for fork with bakefile pipeline defined in per pipeline sections above.
