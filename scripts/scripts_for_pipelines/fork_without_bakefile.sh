#!/usr/bin/env bash
set -xv
#$1 = method either git or direct
#$2 = url
#$3 = version tag if method is git
if [ "${1}" != "git" ] && [ "${1}" != "direct" ]
then
    exit 1
fi
if [ "${1}" = "git" ] && [ $# -lt 3 ]
then
    echo Requires 3 args
    exit 1
fi
if [ "${1}" = "direct" ] && [ $# -lt 2 ]
then
    echo Requires 2 args
    exit 1
fi

mkdir test || exit $?
cd test || exit $?
#get the fork from git or direct
if [ "${1}" = "git" ]
then
    git clone -b $3 --single-branch $2 || exit $?
else
    wget -O fork $2 || exit $?
    file_type=$(file --mime-type -b fork) || exit $?
    if [ "$file_type" = "application/zip" ]
    then
        #.zip
        unzip fork || exit $?
    elif [ "$file_type" = "application/gzip" ]
    then
        #.tar.gz
        tar -xzf fork || exit $?
    elif [ "$file_type" = "application/x-bzip2" ]
    then
        #.tar.bz2
        tar -xjf fork || exit $?
    elif [ "$file_type" = "application/x-tar" ]
    then
        #.tar
        tar -xf fork || exit $?
    else
        echo "Unknown format used"
        exit 1
    fi
    rm fork || exit $?
fi

#find cloned/downloaded folder name/path and clone into it
folder_name=$(ls | head -1) || exit $?
if [ "$folder_name" = "" ]
then
    exit 1
fi
cd $folder_name || exit $?

#configure, build the fork and run tests
./waf -d debug configure --enable-examples --enable-tests --enable-mpi || exit $?
./waf || exit $?
./test.py || exit $?
./waf clean || exit $?
./waf -d optimized configure --enable-examples --enable-tests --enable-mpi || exit $?
./waf || exit $?
./test.py || exit $?
./waf clean || exit $?
./waf -d optimized configure --enable-static --enable-tests --enable-mpi || exit $?
./waf || exit $?
./test.py || exit $?
./waf clean || exit $?
./waf -d release configure --enable-examples --enable-tests --enable-mpi || exit $?
./waf || exit $?
./test.py || exit $?
./waf clean || exit $?