#!/usr/bin/env bash
set -xv
#$1 = module name
#$2 = module release version
#$3 = ns-3 version
if [ $# -lt 3 ]
then
    echo Requires 3 args
    exit 1
fi

#get bake
git clone https://gitlab.com/nsnam/bake.git || exit $?
cd bake || exit $?

#configure, download and build ns-3 first
./bake.py configure -e $3 -m || exit $?
./bake.py --noColor download || exit $?
test -d source/$3 || exit $?
./bake.py --noColor build || exit $?

#get bakefile and create configuration string of modules with bakefile from contrib/
#e.g -e moduleB -e moduleB
./bake.py getconf "${1}==${2}" || exit $?
bakefile_path=$(ls contrib/*.xml| head -1) || exit $?
if [ "$bakefile_path" = "" ]
then
    exit 1
fi
modules=$(python3 ../parse_bake.py $bakefile_path) || exit $?
for module in $modules
do
    conf_string="${conf_string:+$conf_string }-e $module"
done
if [ "$conf_string" = "" ]
then
    exit 1
fi

#configure, download and build the module
./bake.py configure $conf_string -m || exit $?
./bake.py --noColor download || exit $?
./bake.py --noColor build || exit $?
cd source/$3 || exit $?
python3 test.py || exit $?